<?php
header('Content-Type: text/html; charset=utf-8');

$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));

if(PHP_OS == "WINNT") $quebra_linha = "\r\n";
else $quebra_linha = "\n";

if($request[0] == "cadastrar"){
	if(!$_GET['email'] && !$_GET['senha'] && !$_GET['resenha']){
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		$response = array("codigo"=>500,"mensagem"=>"parâmetros inválidos");
		print json_encode($response);
	}
	else if($_GET['email'] && $_GET['senha'] && $_GET['resenha']){
		$mysqli = new mysqli('mysql01.mansioncrossfit.hospedagemdesites.ws', 'mansioncrossfit', 'CfMn1520', 'mansioncrossfit');
		$mysqli->set_charset("utf8");
		
		$email = $_GET['email'];
		$senha = $_GET['senha'];
		$resenha = $_GET['resenha'];
		
		$consulta_permissao = $mysqli->query("SELECT status FROM cfm_tb_user_authorized WHERE email = '$email'");
		$consulta_usuario = $mysqli->query("SELECT status FROM cfm_tb_users WHERE email = '$email'");
		if($consulta_usuario->num_rows == 1){
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			$response = array("codigo"=>500,"mensagem"=>"Erro: usuário já cadastrado.");
			print json_encode($response);
		}
		elseif($senha != $resenha){
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			$response = array("codigo"=>500,"mensagem"=>"Erro: senhas não conferem.");
			print json_encode($response);			
		}
		elseif($consulta_permissao->num_rows == 1){
			$custo = '08';
			$salt = 'Pd7h3lRJ01ql2N7d7Uya5G';
			$hash = crypt($senha, '$2a$' . $custo . '$' . $salt . '$');

			if($mysqli->query("INSERT INTO cfm_tb_users (email, senha, status) VALUES ('$email', '$hash', 0)") == true){
				$headers = "MIME-Version: 1.1".$quebra_linha;
				$headers .= "Content-type: text/html; charset=UTF-8".$quebra_linha;
				$headers .= "From: Admin Crossfit Mansion <contato@crossfitmansion.com.br>".$quebra_linha;
				$headers .= "Return-Path: contato@crossfitmansion.com.br".$quebra_linha;
				
				$emailsender = "contato@crossfitmansion.com.br";

				$custo = '08';
				$salt = 'Pd7h3lRJ01ql2N7d7Uya5G';
				$hash = crypt($email, '$2a$' . $custo . '$' . $salt . '$');

				$mensagemHTML = '
						<div style="width:700px">
							<h1><img height="50" src="http://www.crossfitmansion.com.br/admin/images/logo-horizontal.png"></h1>
							<h2>Clique no link abaixo para validar seu cadastro:</h2>
							<p><a href="http://www.crossfitmansion.com.br/admin/_acesso.php?c='.$hash.'&email='.$email.'">http://www.crossfitmansion.com.br/admin/_acesso.php?c='.$hash.'&email='.$email.'</a></p>
							<br>
							<br>
							<p style="font-size:10px;">Enviado por: crossfitmansion.com.br/admin</p>
						</div>';
			
				if(mail($email, "CFM - Cadastro Admin", $mensagemHTML, $headers, "-r". $emailsender)){
					header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK', true, 200);
					$response = array("codigo"=>200,"mensagem"=>"Cadastro concluido com sucesso","recurso"=> "{ 'id':".$mysqli->insert_id."}");
					print json_encode($response);
				}else{
					header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
					$mysqli->query("DELETE FROM cfm_tb_users WHERE id=$mysqli->insert_id");
					$response = array("codigo"=>500,"mensagem"=>"Não foi possível confirmar seu cadastro. Por favor, tente novamente.");
					print json_encode($response);
				}
			}else{
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
				$response = array("codigo"=>500,"mensagem"=>"Erro: não foi possível cadastrar usuário.", "error"=>$mysqli->error, "query"=>"INSERT INTO cfm_tb_users (email, senha, status) VALUES '$email', '$hash', 0");
				print json_encode($response);
			}
		}else{
			header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
			$response = array("codigo"=>404,"mensagem"=>"Erro: email sem permissão para cadastro.");
			print json_encode($response);
			print mysqli_result($consulta_permissao);
		}
		
		mysqli_close($mysqli);
	}
}
?>
