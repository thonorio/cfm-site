angular.module('cfmAdmin').controller('LoginCtrl', ["LoginService", "$rootScope", LoginCtrl]);

function LoginCtrl(LoginService, $rootScope) {
    var vm = this;
		
    vm.login = function(){
	    var param = {"email": vm.email, "senha": vm.senha};
	    LoginService.getLogin(param).success(function(data){
		   console.log(data); 
	    });
    }

    vm.cadastrar = function(){
	    var param = {"email": vm.email, "senha": vm.senha, "resenha": vm.resenha};
	    LoginService.postCadastro(param).success(function(data){
		   vm.email = vm.senha = vm.resenha = undefined;
		   $rootScope.error = {"type":"success", "mensagem":"Pronto! Para concluir o cadastro confirme no seu link que enviamos pro seu email."}
	    }).error(function(e, errorCode){
		   $rootScope.error = {"type":"error", "mensagem":e.mensagem}		    
	    });
    }
}
