angular.module('cfmAdmin').controller('MainCtrl', [
	"$rootScope",
	"$location",
	"$routeParams",
	"$scope",
	MainCtrl]);

function MainCtrl($rootScope, $location, $routeParams, $scope) {
    var vmMain = this;

	$scope.$on("$routeChangeSuccess", function () {
		if($location.path() != "/") vmMain.topMenu = true;
	});
}
