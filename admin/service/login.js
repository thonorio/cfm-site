angular.module('cfmAdmin').service('LoginService', ["$http", "CFMConfig", LoginService]);

function LoginService($http, CFMConfig) {

	var _getLogin = function(param) {
		return $http({
			url: CFMConfig.serviceEndpoint+'login.php/login',
			method: "GET",
			params: param
		});
	}

	var _postCadastro = function(param){
		return $http({
			url: CFMConfig.serviceEndpoint+'login.php/cadastrar',
			method: "POST",
			params: param
		});
	}
	var _getSenha = function(param){
		return $http({
			url: CFMConfig.serviceEndpoint+'login.php/esqueci-senha',
			method: "POST",
			params: param
		});
	}

	return {
		"getLogin": _getLogin,
		"postCadastro": _postCadastro,
		"getSenha": _getSenha
	}
}
