angular.module('cfmAdmin', ['ngRoute',
							'ui.bootstrap']);

angular.module('cfmAdmin').config(["$routeProvider", ConfigCFMAdmin]);

function ConfigCFMAdmin($routeProvider, $httpProvider) {
	$routeProvider
		.when("/", {
			templateUrl: 'view/login.html',
			controller: 'LoginCtrl',
			controllerAs: "vm"
		})
		.when("/redefinir-senha", {
			templateUrl: 'view/redefinir-senha.html',
			controller: 'LoginCtrl',
			controllerAs: "vm"
		})
		.when("/cadastrar", {
			templateUrl: 'view/cadastrar.html',
			controller: 'LoginCtrl',
			controllerAs: "vm"
		})
		.otherwise("/");
}