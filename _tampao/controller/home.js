angular.module('cfm').controller('homeCtrl', ['SendMailService', homeCtrl]);

function homeCtrl(SendMailService) {
	var vm = this;
	
	vm.cadastrar = function(){
		SendMailService.sendMail(vm.form).success(function(data){
			vm.sended = true;
			vm.success = "Sua mensagem foi enviada com sucesso. Confirme seu desconto na inauguração.";
		}).error(function(statusCode, status){
			vm.error = "Ocorreu um erro ao enviar sua mensagem";
		})
	}
}