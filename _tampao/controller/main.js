angular.module('cfm').controller('MainCtrl', [MainCtrl]);

function MainCtrl() {
    var vmMain = this;
    
    vmMain.deadline = new Date(2016,2,5);

	var today = new Date();

    vmMain.countdown = Math.ceil((Math.abs(today.getTime() - vmMain.deadline.getTime())) / (1000 * 3600 * 24));
}