angular.module('cfm').service('SendMailService', ["$http", SendMailService]);

function SendMailService($http) {
	
	var _sendMail = function(data) {
		return $http({
			url: 'api/sendmail.php', 
			method: "POST",
			data: data
		});
    };

	return {
		"sendMail": _sendMail,
	}
}