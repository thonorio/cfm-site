angular.module('cfm', ['ngRoute', 'ui.utils.masks']);

angular.module('cfm').config(['$routeProvider', "$compileProvider", Config]);

function Config($routeProvider, $compileProvider) {
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);

	$routeProvider
		.when("/", {
			templateUrl: 'views/home.html',
			controller: 'homeCtrl',
			controllerAs: "vm"
		})
		.otherwise("/");
}